// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MilasShooterGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MILASSHOOTER_API AMilasShooterGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	virtual void PawnKilled(APawn* PawnKilled);
};
