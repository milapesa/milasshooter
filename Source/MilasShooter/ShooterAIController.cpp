// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterAIController.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "ShooterCharacter.h"

void AShooterAIController::BeginPlay()
{
	Super::BeginPlay();
	
	if (AiBehavior != nullptr)
	{
		RunBehaviorTree(AiBehavior);
	}
}

void AShooterAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

bool AShooterAIController::IsDead() const
{
	 AShooterCharacter* ControlledCharacter= Cast<AShooterCharacter>(GetPawn());
	 if (ControlledCharacter != nullptr)
	 {
		 return ControlledCharacter->IsDead();
	 }
	 return true;
}